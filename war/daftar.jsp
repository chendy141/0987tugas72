<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="id">
    <head>
        <title>Daftar</title>
    </head>
    <body>
 
        <h1>Daftar</h1>
         <a href="/tambah">Tambahkan Data Mahasiswa</a>
        <table border="1">
            <tr>
                <th>Nama</th>
                <th>NIM</th>
                <th>Email</th>
                <th>NoHP</th>
                <th>Keterangan</th>
                <th>Ubah</th>
                <th>Hapus</th>
            </tr>
<c:forEach var="satu" items="${daftar}">
            <tr>
            	<td>${satu.properties.Nama}</td>
                <td>${satu.properties.NIM}</td>
                <td>${satu.properties.Email}</td>
                <td>${satu.properties.NoHp}</td>
                <td>${satu.properties.Aktif}</td>
                <td><a href="/ubah?id=${satu.key.id}">Ubah</a></td>
                <td><a href="/hapus?id=${satu.key.id}">Hapus</a></td>
            </tr>
</c:forEach>                        
        <table>
        
    </body>
</html>

