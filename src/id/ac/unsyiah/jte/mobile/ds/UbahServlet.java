package id.ac.unsyiah.jte.mobile.ds;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterPredicate;

@SuppressWarnings("serial")
public class UbahServlet extends HttpServlet 
{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws IOException, ServletException 
	{
		// Ambil ID dari data yang mau ditampilkan
		long keyId = Long.valueOf(req.getParameter("id"));
		Key key = KeyFactory.createKey("Daftar Mahasiswa", keyId);

		// Bangun query-nya
		FilterPredicate filter = new Query.FilterPredicate(Entity.KEY_RESERVED_PROPERTY,
				  										   Query.FilterOperator.EQUAL,
				  										   key); 
		Query query = new Query("Daftar Mahasiswa");
		query.setFilter(filter);
		
		// Baca
		DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
		PreparedQuery preparedQuery = datastoreService.prepare(query);
		Entity data = preparedQuery.asSingleEntity();
		
		// Kirim ke halaman
		req.setAttribute("data", data);
		
		// Tampilkan halaman
		resp.setContentType("text/html");
		RequestDispatcher jsp = req.getRequestDispatcher("ubah.jsp");
		jsp.forward(req, resp);		
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException
	{
		// Ambil ID dari data yang mau ditampilkan
		long keyId = Long.valueOf(req.getParameter("hdnId"));
		Key key = KeyFactory.createKey("Daftar Mahasiswa", keyId);

		// Bangun query-nya
		FilterPredicate filter = new Query.FilterPredicate(Entity.KEY_RESERVED_PROPERTY,
				  										   Query.FilterOperator.EQUAL,
				  										   key); 
		Query query = new Query("Daftar Mahasiswa");
		query.setFilter(filter);
		
		// Baca
		DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
		PreparedQuery preparedQuery = datastoreService.prepare(query);
		Entity dataAwal = preparedQuery.asSingleEntity();
		
		// Ambil data baru
		String Nama = req.getParameter("txtNama");
		Long NIM = Long.valueOf(req.getParameter("txtNIM"));
		String Email = req.getParameter("txtEmail");
		Long NoHp = Long.valueOf(req.getParameter("txtNoHp"));
		String Aktif = req.getParameter("txtAktif");
		
		// Ubah
		dataAwal.setProperty("Nama", Nama);
		dataAwal.setProperty("NIM", NIM);
		dataAwal.setProperty("Email", Email);
		dataAwal.setProperty("NoHp", NoHp);
		dataAwal.setProperty("Aktif", Aktif);
		// Tulis
		datastoreService.put(dataAwal);
		
		resp.sendRedirect("/");	
	}
}
